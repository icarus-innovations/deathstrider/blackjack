### Blackjack
---
This is an addon for Deathstrider that adds the BlackJack, a .50AE SMG with an integral underbarrel shotgun. It has a chance to spawn on chaingun spawns.

As of June 13, 2023 Blackjack now has 10mm magazines that can be used. DAX is NOT required but the 10mm magazines will be useless without it. If you do use DAX you'll have to use [Use] + [Alt. Reload] to change magazine types if you have more than 1 of each magazine type. i.e. you have 2 .50AE mags and 3 10mm mags, you'll need to manually switch mag types, otherwise it's handled automatically.

### Credits and Permissions
---
All the cool people who inspired or helped me make this mod:
- Accensus, coding and gauntlet sprites
- Zhs2, coding, troubleshooting and being an all around good dude.
- TSF, Pillow Blaster and Project Brutality, sprite assets
- Icarus, sprite edits. (Yes, I'm crediting myself. Yes, I have a massive ego.)
- Firing sounds from [GameBanana](https://gamebanana.com/sounds/21017)
- Other sounds from [GameBanana](https://gamebanana.com/sounds/22980)

All the assets used in this mod are original or used with permission from the original creator, if you want to use any of the assets in this mod in your own content you must get permission from the orginal creator.